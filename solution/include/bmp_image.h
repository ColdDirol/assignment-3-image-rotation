#ifndef BMP_IMAGE_H
#define BMP_IMAGE_H

#include <stdint.h>

struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint16_t bfReserved1;
    uint16_t bfReserved2;
    uint32_t bOffBits;
    uint32_t biSize;
    int32_t biWidth;
    int32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    int32_t biXPelsPerMeter;
    int32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

struct __attribute__((packed)) pixel {
    uint8_t blue;
    uint8_t green;
    uint8_t red;
};

struct bmp_image {
    struct bmp_header header;
    struct pixel* pixels;
};

struct bmp_image* load_bmp_image(const char* filename);
struct bmp_image* rotate_bmp_image(const struct bmp_image* image, int angle);
void save_bmp_image(const struct bmp_image* image, const char* filename);
void free_bmp_image(struct bmp_image* image);

#endif // BMP_IMAGE_H
