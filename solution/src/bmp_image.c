#include "bmp_image.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct bmp_image* load_bmp_image(const char* filename) {
    FILE* file = fopen(filename, "rb");
    if (file == NULL) {
        return NULL;
    }

    struct bmp_image* image = (struct bmp_image*)malloc(sizeof(struct bmp_image));
    if (image == NULL) {
        fclose(file);
        return NULL;
    }

    fread(&(image->header), sizeof(struct bmp_header), 1, file);

    // Calculate padding
    unsigned long offset = (image->header.biWidth * sizeof(struct pixel)) % 4;
    int padding = offset ? (4 - (int)offset) % 4 : 0;

    image->pixels = (struct pixel*)malloc(image->header.biWidth * image->header.biHeight * sizeof(struct pixel));
    if (image->pixels == NULL) {
        fclose(file);
        free(image);
        return NULL;
    }

    fseek(file, image->header.bOffBits, SEEK_SET);
    for (int row = image->header.biHeight - 1; row >= 0; --row) {
        fread(&(image->pixels[row * image->header.biWidth]), sizeof(struct pixel), image->header.biWidth, file);
        fseek(file, padding, SEEK_CUR);
    }

    fclose(file);

    return image;
}

struct bmp_image* rotate_bmp_image(const struct bmp_image* image, int angle) {
    int width = image->header.biWidth;
    int height = image->header.biHeight;

    struct bmp_image* rotated_image = (struct bmp_image*)malloc(sizeof(struct bmp_image));
    if (rotated_image == NULL) {
        return NULL;
    }

    rotated_image->header = image->header;

    if (angle == 0) {
        // No rotation
        rotated_image->pixels = (struct pixel*)malloc(width * height * sizeof(struct pixel));
        if (rotated_image->pixels == NULL) {
            free(rotated_image);
            return NULL;
        }

        for (int i = 0; i < width * height; ++i) {
            rotated_image->pixels[i] = image->pixels[i];
        }
    } else if (angle == 90 || angle == -270) {
        // 90 degrees clockwise rotation
        rotated_image->header.biWidth = height;
        rotated_image->header.biHeight = width;
        rotated_image->pixels = (struct pixel*)malloc(width * height * sizeof(struct pixel));
        if (rotated_image->pixels == NULL) {
            free(rotated_image);
            return NULL;
        }

        for (int row = 0; row < height; ++row) {
            for (int col = 0; col < width; ++col) {
                rotated_image->pixels[col * height + (height - row - 1)] = image->pixels[row * width + col];
            }
        }
    } else if (angle == -90 || angle == 270) {
        // 90 degrees counterclockwise rotation
        rotated_image->header.biWidth = height;
        rotated_image->header.biHeight = width;
        rotated_image->pixels = (struct pixel*)malloc(width * height * sizeof(struct pixel));
        if (rotated_image->pixels == NULL) {
            free(rotated_image);
            return NULL;
        }

        for (int row = 0; row < height; ++row) {
            for (int col = 0; col < width; ++col) {
                rotated_image->pixels[(width - col - 1) * height + row] = image->pixels[row * width + col];
            }
        }
    } else if (angle == 180 || angle == -180) {
        // 180 degrees rotation
        rotated_image->pixels = (struct pixel*)malloc(width * height * sizeof(struct pixel));
        if (rotated_image->pixels == NULL) {
            free(rotated_image);
            return NULL;
        }

        for (int row = 0; row < height; ++row) {
            for (int col = 0; col < width; ++col) {
                rotated_image->pixels[(height - row - 1) * width + (width - col - 1)] = image->pixels[row * width + col];
            }
        }
    } else {
        free(rotated_image);
        return NULL; // Invalid angle
    }

    return rotated_image;
}


void save_bmp_image(const struct bmp_image* image, const char* filename) {
    FILE* file = fopen(filename, "wb");
    if (file == NULL) {
        return;
    }

    // Update new bitmap info.
    struct bmp_header new_header = image->header;
    new_header.bfileSize = 54 + image->header.biHeight * ((image->header.biWidth * sizeof(struct pixel) + 3) & ~3); // 54 is header size
    new_header.bOffBits = 54;
    new_header.biSizeImage = new_header.bfileSize - new_header.bOffBits;

    fwrite(&new_header, sizeof(struct bmp_header), 1, file);

    unsigned long offset = (image->header.biWidth * sizeof(struct pixel)) % 4;
    int padding = offset ? (4 - (int)offset) % 4 : 0;

    for (int row = image->header.biHeight - 1; row >= 0; --row) {
        fwrite(&(image->pixels[row * image->header.biWidth]), sizeof(struct pixel), image->header.biWidth, file);

        // Write padding
        uint8_t padding_byte = 0;
        for(int i = 0; i < padding; i++) {
            fwrite(&padding_byte, 1, 1, file);
        }
    }

    fclose(file);
}

void free_bmp_image(struct bmp_image* image) {
    free(image->pixels);
    free(image);
}
