#include "bmp_image.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
    if (argc != 4) {
        printf("Usage: %s <source-image> <transformed-image> <angle>\n", argv[0]);
        return 1;
    }

    const char* source_image_filename = argv[1];
    const char* transformed_image_filename = argv[2];
    int angle = atoi(argv[3]);

    if (angle != 0 && angle != 90 && angle != -90 && angle != 180 && angle != -180 && angle != 270 && angle != -270) {
        printf("Invalid angle. Angle must be one of the following values: 0, 90, -90, 180, -180, 270, -270.\n");
        return 1;
    }

    struct bmp_image* source_image = load_bmp_image(source_image_filename);
    if (source_image == NULL) {
        printf("Failed to load source image: %s\n", source_image_filename);
        return 1;
    }

    struct bmp_image* transformed_image = rotate_bmp_image(source_image, angle);
    if (transformed_image == NULL) {
        printf("Failed to transform image.\n");
        free_bmp_image(source_image);
        return 1;
    }

    save_bmp_image(transformed_image, transformed_image_filename);

    free_bmp_image(source_image);
    free_bmp_image(transformed_image);

    return 0;
}
